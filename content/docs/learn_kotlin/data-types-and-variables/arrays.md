---
title: 'Arrays'
order: 8
---

## Introduction to arrays

When you need to process multiple values of the same type, you can save them in an **array**.
Then, you can process them together as a single unit.
It is a very convenient approach if you don't know how many elements the program will process during runtime.

You may consider an **array** as a collection of elements of the same type.
All the elements are stored in the memory sequentially.

The collection provides one name for its elements.
The possible number of elements to be stored is established when the array is created and it cannot be changed.
However, you can modify a stored element at any time.

The picture below illustrates an array of five floating-point numbers.
Each element has an integer index (0-4), so you can access it.
It is important that the first element has the index `0`, the last element has the index that equals `array size - 1`.

![Group](./assets/table.png)

We hope you got the general idea of what arrays are. Now let's see how to use them in Kotlin.

## Creating an array with specified elements

Kotlin provides many types for representing arrays:
`IntArray`, `LongArray`, `DoubleArray`, `FloatArray`, `CharArray`, `ShortArray`, `ByteArray`, `BooleanArray`.
Each array stores elements of the corresponding type (`Int`, `Long`, `Double`, and so on).
Note, there is no `StringArray`.

To create an array of a specified type,
we need to invoke a special function and pass all the elements to store them together:

- `intArrayOf` creates `IntArray`;
- `charArrayOf` creates `CharArray`;
- `doubleArrayOf` creates `DoubleArray`;
- and so on.

For example, let's create three arrays:

```kotlin
val numbers = intArrayOf(1, 2, 3, 4, 5) // It stores 5 elements of the Int type
println(numbers.joinToString()) // 1, 2, 3, 4, 5

val characters = charArrayOf('K', 't', 'l') // It stores 3 elements of the Char type
println(characters.joinToString()) // K, t, l

val doubles = doubleArrayOf(1.25, 0.17, 0.4) // It stores 3 elements of the Double type
println(doubles.joinToString()) // 1.15, 0.17, 0.4
```

This code snippet prints three arrays:

```
1, 2, 3, 4, 5
K, t, l
1.25, 0.17, 0.4
```

The `joinToString()` function converts an array to a string to print.

## Creating an array with a specified size

To create an array with a specified size,
we need to write its type and pass it after the type's name in round brackets (constructor):

```kotlin
val numbers = IntArray(5) // an array for 5 integer numbers
println(numbers.joinToString())

val doubles = DoubleArray(7) // an array for 7 doubles
println(doubles.joinToString())
```

These arrays with the predefined size are filled by default values of the corresponding types (zeros for numeric types):

```
0, 0, 0, 0, 0
0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
```

Though you cannot change the size of an array, you can modify the elements.

## The size of an array

An array always has a size, that is, the number of elements.
To obtain it, we need to take the value of the `size` property. It is a number of the `Int` type.

```kotlin
val numbers = intArrayOf(1, 2, 3, 4, 5)
println(numbers.size) // 5
```

> You cannot change the size of an array after it has been created.

## Accessing elements

You can change the values of elements of an array. To set (get) a value in (from) the array, you can use the index.

Set the value by the index:

```kotlin
array[index] = elem
```

Get the value by the index:

```kotlin
val elem = array[index]
```

Indexes of arrays are numbers from `0` (the first element) to `array.size-1` (the last element).

Here is an example with a 3-element array of integers:

```kotlin
val numbers = IntArray(3) // numbers: 0, 0, 0

numbers[0] = 1 // numbers: 1, 0, 0
numbers[1] = 2 // numbers: 1, 2, 0
numbers[2] = numbers[0] + numbers[1] // numbers: 1, 2, 3

println(numbers[0]) // 1, the first element
println(numbers[2]) // 3, the last element
```

Let's take a closer look at the code above. First, there is an array with three elements.
By default, all its elements are equal to 0.
Then, the value 1 is assigned to the first element of the array by the index 0.
Then, the value 2 is assigned to the second element of the array by the index 1.
After, the value 3 (the sum of 1 and 2) is assigned to the last element of the array by the index 2.
Then we print the first and the last element of the 3-element array.

Note, if we try to access a non-existing element by the index, the program will throw an exception.
Let's try to get the fourth element with the index 3 of the considered array `numbers`:

```kotlin
val elem = numbers[3]
```

The program throws `ArrayIndexOutOfBoundsException`.

> Be careful while indexing the elements of an array.

As you may already know, the last element has the index that equals `array size - 1`.
Let's access the last element and the one before last:

```kotlin
println(strings.first()) // "abc"
println(strings.last()) // "efg"
println(strings.lastIndex) // 2
```

Use this approach to make your code more readable and prevent accessing non-existent indexes.

## Comparing arrays

To compare two arrays, invoke the `contentEquals()` function of an array, passing another one as the argument.
This function returns `true` when two arrays contain exactly the same elements in the same order,
otherwise, it returns `false`:

```kotlin
val numbers1 = intArrayOf(1, 2, 3, 4)
val numbers2 = intArrayOf(1, 2, 3, 4)
val numbers3 = intArrayOf(1, 2, 3)

println(numbers1.contentEquals(numbers2)) // true
println(numbers1.contentEquals(numbers3)) // false
```

Do not use the operators `==` and `!=` for comparing arrays. They do not compare the contents of arrays.

## Conclusion

We learned what an array is and what basic operations it provides. Remember these points:

- an array is a collection of elements accessed by indexes;
- the first element of an array has the index 0;
- an array has a size;
- it is possible to modify an element of an array by its index;
- Kotlin has arrays of different types such as `IntArray`, `CharArray`, `DoubleArray`, and so on.
