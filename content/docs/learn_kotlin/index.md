---
title: 'Learn Kotlin'
description: ''
order: 0
---

## The first look at Kotlin

- [Introduction to Kotlin](the-first-look-at-kotlin/introduction_to_kotlin)
- [Basic literals](the-first-look-at-kotlin/basic_literals)
- [Overview of the basic program](the-first-look-at-kotlin/overview_of_the_basic_program)

## Data types and variables

- [Values and variables](data-types-and-variables/values_and_variables)
- [Data types](data-types-and-variables/data_types)
- [The classification of basic types](data-types-and-variables/the_classification_of_basic_types)
- [Type conversion](data-types-and-variables/type_convension)
- [Type inference](data-types-and-variables/type_inference)
- [Ranges](data-types-and-variables/ranges)
- [Objects](data-types-and-variables/objects)
- [Arrays](data-types-and-variables/arrays)
- [Nullable and non-nullable types](data-types-and-variables/nullable_and_non_nullable_types)
- [Types system](data-types-and-variables/type_system)

## Code style

- [Comments](code-style/comments)
- [Coding style conventions](code-style/coding_style_conventions)
- [Naming variables](code-style/naming_variables)
