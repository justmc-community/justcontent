---
title: 'Overview of the basic program'
order: 3
---

In this lesson, you will write your first Kotlin program, which prints `"Hello, World!"` This is the first step that
every beginner is to take. Although the program itself is very simple, it is still a working program and it shows the
basic syntax of a programming language.

## The Hello World program

Here it comes. It is the source code of this program:

```kotlin
fun main() {
    println("Hello, World!")
}
```

To start the program, click the triangle on the right. It outputs `"Hello, World!"`.

If you have already installed the programming environment on your computer, you may run the program there. If not, do
not worry. We will get back to this.

## The basic terminology

After you have seen the result, let's learn the basic terminology and then try to understand our program.

- **program** is a sequence of instructions (called statements), which are executed one after another in a predictable
  manner. Sequential flow is the most common and straightforward way, where statements are executed according to their
  order, i.e., from top to bottom one after the other;
- **statement** (or **programming statement**) is a single action (like printing a text);
- **block** is a group of zero or more statements enclosed by a pair of braces `{...}`; our program has a single block;
- **keyword** is a word that has a special meaning in the programming language. Names of keywords are constant;
- **identifier** (or **name**) is a word written by a programmer to identify something;
- **comment** is a piece of text that is ignored when executing the program, but it is to explain a part of the
  program. Comment start with `//`;
- **whitespace** is a blank, tab, or a newline; they are used to separate words in the program and to improve
  readability.

## The Hello World program under a microscope

The **Hello World** program illustrates the basic elements of any Kotlin program. Right now, we will consider only the
most important ones.

The **entry point**. The keyword `fun` defines a function that contains a piece of code to execute. This function has a
special name `main`. It indicates the entry point for a Kotlin program. The function has a body enclosed inside braces
`{...}`.

```kotlin
fun main() {
    // ...
}
```

We will discuss the functions later. The name of this function should always be the same: `main`. If you name it `Main`
or `MAIN` or something else, the program will not start.

> **Note**: The text after `//` is just a comment, not a part of the program. Further, we will learn how to write comments.

**Printing "Hello, World!"**. The body of this function consists of programming statements that define what the program
is to do. Our program prints the string **"Hello, World!"** using the following statement:

```kotlin
println("Hello, World!")
```

This is one of the most important things to understand about the **Hello World program**. We invoke the special function
`println` to display the string followed by a new line on the screen. We will often use this way to print something.

> **Remember** that `"Hello, World!"` is not a keyword or a name, it is just a string literal to print on the screen.

## Programs with multiple statements

As a rule, a program contains multiple statements. You should start a new line to write a statement. Oh, look: the
program below has two statements.

```kotlin
fun main() {
    println("Hello")
    println("World")
}
```

If you run the program (clicking the green triangle), you will see that it displays this:

```
Hello
World
```

## Conclusion

Congratulations! We have written our first program, which prints **"Hello, World!"** It has one function named `main`
which represents the entry point for this program. Don't worry about all those terms
(**syntax**, **statement**, **block**) for now. We will explain them throughout this course. Don’t forget to use your
first program as a template for your future programs.
