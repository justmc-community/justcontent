---
title: 'Coding style conventions'
order: 2
---

## Conventions

It is a well-known fact that programmers spend a lot of time reading code that was written by others.
That is why it is so important to write clear code.

If you have a team of several programmers, all the code should look as if only one person wrote it.
In this case, it will be easy for others to read it.

_Code style conventions_ are here to help us standardize and support well-readable code.
They are more like recommendations than strict rules.
If a programmer follows them, their code will be cleaner and more consistent.

Kotlin has its own conventions, which can be found [here](https://kotlinlang.org/docs/reference/coding-conventions.html).
We will follow them in all our examples and exercises, and we recommend you to do the same.
There is no need to learn all the conventions at once;
just open them from time to time after learning some new concepts.
We will also give pieces of these conventions during this course.

## Follow your first conventions

Let's learn a few code conventions for Kotlin.

- Use **4 spaces** for **indentation** instead of tabs.
- **Omit semicolons** (`;`) whenever possible; in many cases, they are optional.
- Put the **opening curly brace** at the **end of the line** where the block begins.
- Put the **closing curly brace** at the **beginning of the next line**.

For example, the program bellow follows those rules:

```kotlin
fun main() { // opening curly brace
    println("Hi") // this statement is offset by 4 spaces and has no `;` at the end
} // closing brace
```

Congratulations, you are already following the conventions! Keep on, and you'll always write wonderful code.
And remember: _good code is like a good joke: it needs no explanation_.
